#define WIFI_SSID "MTS_Router_168"
#define WIFI_PASS "86817608"
#define BOT_TOKEN "6171294672:AAFNhT9i-QzcgGnEmKUffnS3C0_fqmib_qw"

#include <ESP8266WiFi.h>
#include <FastBot.h>
#include "GyverStepper2.h"

FastBot bot(BOT_TOKEN);
GStepper2< STEPPER2WIRE> stepper(2048, D6, D7, D5);

bool dir = 1;   // направление
int pos = 0;    // позиция
int coef = 360; // коэффициент
bool auto_ = false;
bool click = false;
bool flag = false;
bool temp = false;
uint32_t btnTimer = 0;
String chat_id = "990674319";


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(D1, INPUT_PULLUP);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);

  pinMode(D6, OUTPUT);
  digitalWrite(D6, LOW);

  stepper.setAcceleration(1000);
  stepper.setMaxSpeed(650);   // скорость движения к цели
  stepper.setTargetDeg(0);    // начинаем движение с позиции 0
  stepper.autoPower(true);

  WiFi.mode(WIFI_STA);
  connectWifi();

  bot.attach(newMsg);
}

void loop() {
  bot.tick();
  stepper.tick();

  bool btnState = !digitalRead(D1);
  if (btnState && !flag && millis() - btnTimer > 100) {
    flag = true;
    btnTimer = millis();
  }

  if (btnState && flag && millis() - btnTimer > 2000) {
    btnTimer = millis();
    temp = true;
  }

  if (!btnState && flag && millis() - btnTimer > 2000) {
    flag = false;
    btnTimer = millis();

    if (temp == true) {
      auto_ = !auto_;

      if (auto_ == true) {
        bot.sendMessage("Автоматический режим кормления включен!", chat_id);
      } else {
        bot.sendMessage("Автоматический режим кормления выключен!", chat_id);
      }
    } else {
      click = true;
      bot.sendMessage("Рыбки покормлены.", chat_id);
    }
    temp = false;
  }
  
  if (click) {
    stepper.enable();

    if (stepper.ready()) {
      rotate();
    }
    click = false;
  }

  if (auto_) {
    digitalWrite(D3, HIGH);
    stepper.enable();
    static uint32_t tmr;

    if (!stepper.tick()) {
      if (millis() - tmr >= 28800000) {
        digitalWrite(D6, HIGH);
        stepper.enable();
        tmr = millis();
        rotate();
        bot.sendMessage("Рыбки покормлены.", chat_id);
        digitalWrite(D6, LOW);
      }
    }
  } else {
    digitalWrite(D3, LOW);
  }
}

void connectWifi() {
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(D2, HIGH);
    delay(300);
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(D2, LOW);
    delay(300);
    if (millis() > 15000) ESP.restart();
  }
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(D2, HIGH);
}

void newMsg(FB_msg& msg) {
  if (msg.text == "Покормить") {
    stepper.enable();
    rotate();
    bot.sendMessage("Рыбки покормлены.", msg.chatID);
  }

  if (msg.text == "Авто") {
    bot.sendMessage("Автоматический режим кормления включен!", msg.chatID);
    auto_ = true;
  }

  if (msg.text == "Выключить") {
    bot.sendMessage("Автоматический режим кормления выключен!", msg.chatID);
    auto_ = false;
  }
}

void rotate() {
  pos = pos + coef;
  stepper.setTargetDeg(pos);
  coef = coef * -1;
}